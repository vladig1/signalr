package web.chat.example.webchat.models;

public class ChatMember {
    private String id;
    private String name;

    public ChatMember(String id, String name) {
        this.id = id;
        this.name = name;
    }
}
