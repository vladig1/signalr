package web.chat.example.webchat.models;

import java.util.*;

public class ChatEvent {
    private UUID id = UUID.randomUUID();
    public String type;
    private String textFormat = "plain";
    private String language;
    private ChatMember from;
    private ChatMember recepient;
    private List<ChatMember> membersAdded;
    private String messageType = "text";
    private String locale = "en-US";
    private String timestamp = "2024-02-05T22:50:58.332Z";
    private String localTimezone = "Europe/Sofia";
    private String localTimestamp = "2024-02-05T22:50:58.332Z";
    public String text;
    private String mimeType;
    private String accessToken;

    public ChatEvent(String text, String type) {
        this.text = text;
        this.mimeType = "text";
        this.type = type;
        this.language = "EN";
        this.from = new ChatMember("test-user-id","test-user-id");
        this.recepient = new ChatMember("beth-assistant","beth-assistant");
        this.membersAdded = new ArrayList<>();
        this.membersAdded.add(this.from);
        this.membersAdded.add(this.recepient);
        this.accessToken = "";
    }
}
