package web.chat.example.webchat;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Scanner;

import web.chat.example.webchat.models.ChatEvent;

import com.microsoft.signalr.HubConnection;
import com.microsoft.signalr.HubConnectionBuilder;
import com.microsoft.signalr.TransportEnum;

@SpringBootApplication
public class WebchatApplication {

	public static void main(String[] args) {
		SpringApplication.run(WebchatApplication.class, args);

        HubConnection hubConnection = HubConnectionBuilder.create("ws://localhost:5009/webchat")
                .withTransport(TransportEnum.WEBSOCKETS)
                .withKeepAliveInterval(100)
                .shouldSkipNegotiate(true)
                .build();

        hubConnection.on("ServerMessage", message -> {
            System.out.println("New Message: " + message.text);
        }, ChatEvent.class);
        
        //This is a blocking call
        hubConnection.start().blockingSubscribe();
        
        ChatEvent chatEventInit = new ChatEvent(null, "conversationUpdate");
        hubConnection.invoke("chatEvent", chatEventInit);

        Scanner reader = new Scanner(System.in);  // Reading from System.in

        while (true){
            String input = reader.nextLine();

            if (input.equals("exit")) {
                break;
            }

            ChatEvent chatEvent = new ChatEvent(input, "message");
            hubConnection.invoke("chatEvent", chatEvent);
        }

        hubConnection.stop();
	}
}
